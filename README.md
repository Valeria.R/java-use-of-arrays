//Array Assignment
//By Valeria Ramirez
//Wednesday, May 29, 2019
//Version 1.0
/* This program was created to demonstrate an understanding an capability of 
using arrays, random numbers, and differenct loops. The program also shows use 
of different classes and how they are applied.
It starts off by prompting the user for a number length. This will be used to 
form a random array. They will also be asked a target number. The conclusion of 
the program is to produce a random number array explaining how many numbers are
greater and less and their target.*/